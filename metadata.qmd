--- 
title: "Métadonnées"

date: false
---


::: {.panel-tabset .nav-pills}

### Jeu de données/enquête

* **Nom de l'enquête**: Étude des relations familiales et intergénérationnelles (ERFI) - Vague 1

* **Date de passation**: 2005

* **Organisme(s) producteur(s)**:
  * [Institut National des Etudes Démographiques](https://www.ined.fr/)
  * [Institut National de la Statistique et des Études Économiques](https://www.insee.fr/)

* **Univers**: Femmes et hommes âgés de 18 à 79 ans inclus (âge au 31/12/2005), vivants en résidence principale en France métropolitaine dans un ménage ordinaire 
* **Taille de l'échantillon**: 10 079

* **Inscription de l'enquête dans un programme international**: Version française du programme [*Generations and Gender Survey* (GGS)](https://www.ggp-i.org/)

* **FPA et métadonnées** : A venir

* **Jeu de données pseudonymisées**: Disponible *via* [Quetelet-Progedo-Diffusion](https://data.progedo.fr/studies/doi/10.48756/ined-IE0229-1380)


### Approche pédagogique

* **Pré-requis**:  Aucun

* **Objectif(s) pédagogique(s)**:  

1.    Mettre en lumière les opportunités de recherche en sciences humaines et sociales offertes par les bases de données collectées dans le cadre de LifeObs, en prenant pour exemple l'enquête européenne « Étude des relations familiales et intergénérationnelles (ERFI) », réalisée par l'INED et l'INSEE en 2005. Cette présentation a pour objectif de faciliter la prise en main des données du cycle d'enquêtes ERFI-2, dont la collecte a débuté en 2023. Les Fichiers de Production et de Recherche (FPR) tirés de ERFI-2 seront bientôt accessibles gratuitement aux chercheurs via l'application Quetelet-Progedo-Diffusion ;   
2.    Sensibiliser les participants à l'exploitation de données d'enquêtes réelles et à ses particularités à partir de la réplication des premiers résultats obtenus à partir des données de l'enquête ERFI-1 publiés dans un article écrit par Arnaud Régnier-Loilier en 2006 dans la revue *Population et Sociétés*: ["À quelle fréquence voit-on ses parents?"](https://www.ined.fr/fr/publications/editions/population-et-societes/a-quelle-frequence-voit-on-ses-parents/) ;  
3.    Initier les participants à l'utilisation du langage R et de son interface R-Studio pour le traitement statistique de données d'enquêtes.


* **Type de support**: Auto-formation

:::
