---
title: "Initiation à l'exploitation de données d'enquête à partir du langage R - ERFI-1"
subtitle: false

author: 
 - name: "Maude Crouzet"
   affiliation: "PUD de Strasbourg (PUD-S)"
   
 - name: "Claire Kersuzan"
   affiliation: "PUD de Bordeaux (PUD-Bx)"   


toc: false
---


<!-- NORMALEMENT UNE SECTION QUI NE SERA PAS MODIFIE -->
## Le projet Lifeobs

::: {.box_img}
![](img1/logo_lo.png)
:::

[LifeObs](https://lifeobs.site.ined.fr/), l’Observatoire français des parcours de vie, est une infrastructure de recherche qui vise à développer un programme d’enquêtes longitudinales et innovantes sur les comportements familiaux, à accroître la diffusion des données, et à former les utilisateurs.
LifeObs, lauréat du programme [Équipements structurants pour la recherche » du PIA3 en 2020](https://www.enseignementsup-recherche.gouv.fr/fr/resultats-de-l-appel-manifestations-d-interet-ami-equipements-structurants-pour-la-recherche-esr-47178), est financé sur huit ans à partir de février 2021.
<br>

::: {style="margin-top: 30px"} 
::: {.columns}

::: {.column width="15%"}
![](img1/logo_anr.png){width=75%} 
:::

::: {.column width="85%"} 
>Ce travail a bénéficié d'une aide de l’État gérée par l'Agence Nationale de la Recherche au titre de France 2030 portant la référence ANR-21-ESRE-0037.   
:::

:::
:::

<!-- PERSONNES CHARGEES DE LA FORMATION -->

## Formateurs

* {{< fa solid chalkboard-user>}} **Maude Crouzet**:
  * {{< fa solid building-columns>}} Plateforme Universitaire de Données de Strasbourg (PUD-S)
  *  {{< fa solid envelope>}} <m.crouzet@unistra.fr>  

<br>

* {{< fa solid chalkboard-user>}} **Claire Kersuzan**:
  * {{< fa solid building-columns>}} Plateforme Universitaire de Données de Bordeaux (PUD-Bx)
  * {{< fa solid envelope>}} <claire.kersuzan@u-bordeaux.fr>






