---
title: "L'enquête Etude des relations familiales et intergénérationnelles (ERFI)"
date: false
---

### Présentation générale

L'enquête *[Etude des relations familiales et intergénérationnelles (ERFI)](https://erfi.site.ined.fr/)* est la branche française du programme européen *[Generations and Gender Programme (GGP)](https://www.ggp-i.org/)*. Mis en place en 2000 à l'initiative de la *Population Activities Unit* de la commission économique des Nations Unies pour l'Europe, ce programme vise à collecter des informations afin d'éclairer les **évolutions récentes du paysage sociodémographique** : vieillissement de la population, baisse de la fécondité, fragilisation des unions, féminisation du marché du travail... Ces informations sont précieuses pour comprendre les enjeux que soulèvent ces transformations, comme la question des solidarités intergénérationnelles ou la redéfinition des rôles des hommes et des femmes dans la société ou au sein des couples.\
Des données ont été collectées selon une méthodologie similaire dans 24 pays (principalement en Europe), avec un questionnaire standardisé, permettant une comparaison internationale. Il est ainsi possible de mesurer l'impact des contextes nationaux et des politiques publiques sur les comportements démographiques.\
Il s'agit d'un **programme d'enquêtes longitudinales**, c'est-à-dire qui réinterroge les mêmes individus à plusieurs intervalles de temps (trois vagues en France : 2005, 2008 et 2011). Cela permet de mesurer l'évolution des situations, ou encore les décalages entre intentions et réalisations.

### Thématiques de l'enquête

Comme son nom l'indique, **le programme GGP porte un intérêt particulier aux problématiques liées au genre et aux relations entre générations**. Ces phénomènes peuvent être étudiés à un instant donné, mais également dans le temps grâce à la dimension longitudinale de l'enquête.

A titre d'exemple, voici une sélection d'articles publiés mobilisant les données des enquêtes GGP dans les différents pays participants au programme, pour illustrer la diversité des thèmes que celles-ci permettent de couvrir :

-   **Autonomie des jeunes adultes :**
    -   Schwanitz K. *et al.*, 2021, [Unpacking the intentions to leave the parental home in Europe using GGS](https://www.demographic-research.org/volumes/vol45/2/45-2.pdf), *Demographic Research* vol. 45/2, pp. 17-54\
    -   Sébille P., 2009, [Un passage vers l'âge adulte en mutation](https://books.openedition.org/ined/5088), *in.* Régnier-Loilier A. (dir), *Parcours de famille*, Paris, Ined, pp. 315-340\
-   **(In)fécondité :**
    -   Peri-Rotem N., 2020, [Ecarts de fécondité en fonction du niveau d'instruction : le rôle de la religion](https://www.cairn.info/revue-population-2020-1-page-9.htm), *Population* 2020/1, vol. 75, pp. 9-38\
    -   Albertini M. et Brini E., 2021, [I've changed my mind. The intentions to be childless, their stability and realisation](https://www.tandfonline.com/doi/full/10.1080/14616696.2020.1764997), *European Societies*, vol. 23 n°2, pp 119-160\
    -   Beaujouan E., 2011, [La fécondité des deuxièmes unions en France : âges des conjoints et autres facteurs](https://www.cairn.info/resume.php?ID_ARTICLE=POPU_1102_0275&contenu=article), *Population*, 2011/2, vol. 66, pp. 275-311\
    -   Tanskanen A. et Rotkirch A., 2014, [The impact of grandparental investment on mother's fertility intentions in four European countries](https://www.demographic-research.org/articles/volume/31/1/), *Demographic Research*, vol. 31/1, pp. 1-26\
-   **Formation et séparation des couples :**
    -   Marteau B., 2019, [Séparation chez les couples corésidents de même sexe et de sexe différent](https://www.cairn.info/revue-population-2019-4-page-521.htm), *Population* 2019/4, vol. 75, pp. 521-549\
    -   Régnier-Loilier A., 2016, [Le devenir conjugal des personnes en relation non cohabitante](https://books.openedition.org/ined/4788), *in.* Régnier-Loilier A. (dir), *Parcours de famille*, Paris, Ined, pp. 111-135\
    -   Beaujouan E., [Fréquence des désaccords, satisfaction dans le couple et séparation](https://books.openedition.org/ined/4793), *in.* Régnier-Loilier A. (dir), *Parcours de famille*, Paris, Ined, pp. 137-160\
    -   Rault W. et Letrait M., 2010, [Formes d'union différentes, profils distincts ? Une comparaison des pacsé.e.s en couple de sexe différent et des marié.e.s](https://www.cairn.info/revue-sociologie-2010-3-page-319.htm?contenu=article), *Sociologie*, 2010/3, vol. 1, pp. 319-336\
-   **Parcours professionnels et familiaux :**
    -   Renaut S. *et al.*, 2016, [Fin d'activité et passage à la retraite, une période empreinte d'incertitude](https://books.openedition.org/ined/4883), *in.* Régnier-Loilier A. (dir), *Parcours de famille*, Paris, Ined, pp. 359-382\
    -   Makay Z., 2016, [Congé parental et interruption de l'activité professionnelle des mères en France et en Hongrie](https://books.openedition.org/ined/4873), *in.* Régnier-Loilier A. (dir), *Parcours de famille*, Paris, Ined, pp. 333-357\
-   **Viellissement et échanges entre générations :**
    -   Le Goff M. *et al.*, 2016, [L'influence des étapes de la vie sur les transferts des parents aux enfants](https://books.openedition.org/ined/4908), *in.* Régnier-Loilier A. (dir), *Parcours de famille*, Paris, Ined, pp. 385-402\
    -   Lefèvre C. *et al.*, 2009, [Les différences d'opinion entre la France et la Russie sur le soutien entre générations](https://books.openedition.org/ined/5173), *in.* Régnier-Loilier A. (dir), *Portraits de famille*, Paris, Ined, pp.515-536\
    -   Ogg J., et Renaut S., 2008, [Enfants du baby-boom et parents vieillissants : des valeurs et des attitudes contingentes au parcours de vie](https://www.cairn.info/revue-gerontologie-et-societe-2008-4-page-129.htm?contenu=article), *Gérontologie et Société*, vol. 31 n°127, pp.129-158\
    -   Steinback A. et Hank K., 2016, [Intergenerational relations in older stepfamilies: a comparison of France, Germany, and Russia](https://pubmed.ncbi.nlm.nih.gov/27117270/), *Journal of Gerontology Series B*, vol. 71 n°5, pp. 880-888\
-   **Rôles de genre au sein des couples :**
    -   Riederer B. *et al.*, 2019, [Fertility intentions and their realisation in couples: how the division of household chores matters](https://journals.sagepub.com/doi/pdf/10.1177/0192513X19848794), *Journal of Family Issues*, vol. 40 n° 13, pp. 1860-1882\
    -   Bauer D., 2009, [L'organisation des tâches domestiques et parentales dans le couple](https://books.openedition.org/ined/5053), *in.* Régnier-Loilier A. (dir), *Portraits de famille*, Paris, Ined, pp. 219-239\
    -   Dereuddre R. *et al.*, 2017, [Power and the gendered division of contraceptive use in Western European couples](http://www.sciencedirect.com/science/article/pii/S0049089X16306871), *Social Science Research*, vol. 64, pp. 263-276

L'ensemble des thématiques abordées ainsi que les questionnaires des enquêtes peuvent être consultés sur le [site de l'enquête ERFI-1](https://erfi.site.ined.fr/fr/les-thematiques/)\
La liste des publications réalisées à partir des données des enquêtes GGP à l'international peut être consultée sur le [site du programme GGP](https://www.ggp-i.org/form/publications/)

### Champ

**L'enquête porte sur les personnes âgées de 18 à 79 ans: 10 079 personnes âgées de 18 à 79 ans vivant en France métropolitaine ont été interrogées**. Ce champ permet de saisir les relations intergénérationnelles dans toute leur diversité, en les mesurant à la fois du point de vue de l'aidant et de celui de l'aidé. Il permet également des comparaisons des comportements familiaux au fil des générations (par exemple le départ du foyer parental, l'entrée dans la vie active, la mise en couple, la première naissance...). Enfin, il offre aussi un focus sur la génération dite "pivot", c'est-à-dire qui peut apporter de l'aide à la fois à ses enfants qui entrent dans la vie adulte, et à ses parents devenus dépendants.\
Il s'agit de la seule enquête longitudinale en Europe qui couvre tous les âges de la vie adulte. C'est également une des rares enquêtes à l'échelle européenne à porter la focale sur les comportements démographiques.

Le programme GGP a été relancé en 2020 avec un nouveau cycle d'enquêtes sur un nouveau panel mais avec un questionnaire très proche de celui du premier cycle et une méthodologie similaire. En France, la collecte de la première vague de [ERFI-2](https://erfi2.site.ined.fr/) a été lancée en 2023.